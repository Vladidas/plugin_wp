<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'wordpress');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'z3qByXt5K}&-@rd%7N-6M212MNSG+V9vidFP6T~%kcv]FK7FZVy-_)A*k,1WZsGp');
define('SECURE_AUTH_KEY',  '!O:^?D3(0=1,.e#+So8,}v0T=b8+bN8mE)NPpCh  Im$zE]8T`8=zNeeJ#`G>,q}');
define('LOGGED_IN_KEY',    '0X=-y,hXNpFyVls{nPvu]pTZL}Pey5z=q5Ds/$2L|Qr^-Jor fri-{Qv!|TJ=pII');
define('NONCE_KEY',        '$zty4LmpzCY-Vz7T08FP2X9EZ!vUBFkmP)Kv(IX*emV){Nn[.)nOz7E(^pnb/ct>');
define('AUTH_SALT',        'FMEF}[=P|^Qr@q{Nu:UXan>$65XGDeU-I[[7xAO1ZQY`,)jH5BuN<W6sWQ%WV>ao');
define('SECURE_AUTH_SALT', 'aH*h,2RK^pj%rQTC}b{kfOAS}459L:MHW(W)qo%M 5=3XbFx#XE|#4 ui`-+f<u-');
define('LOGGED_IN_SALT',   'u|7QDrUt;aY<DJe!{Z6d*bz[0SW2{{1lmj<9$gKs/]o>,>Po +P3fDy]0mf2dgZh');
define('NONCE_SALT',       '^NM6znt97|67>[VOK|Z:Yp=o#v|Eq0:IuJ/]E|@_ :lgY[noQjhi-#bd-ch+?q$I');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
