<style>
    table tbody tr td.js_image img {
        width: 100px;
        height: 100px;
    }
    
    .save-done {
        display: none;
    }
</style>

<section>
    <form>
        <input type="file" name="images[]" multiple>
    </form>

    <table id="gallery">
        <thead>
        <tr>
            <td>
                Картинка
            </td>
            <td>
                Відображати
            </td>
            <td>
                Категорія
            </td>
            <td>
                Дія
            </td>
        </tr>
        </thead>
        <tbody>
        <tr style="display: none">
            <td class="js_image"><img src=""></td>
            <td class="js_show"><input type="checkbox"></td>
            <td class="js_category">
                <select name="category_id" >
                    <option selected>Без категорії</option>
                    <?php if ($categories): ?>
                        <?php foreach ($categories as $category): ?>
                            <option><?= $category->name ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </td>
            <td class="js_action"><button>x</button></td>
        </tr>

            <?php foreach ($textures as $texture): ?>
                <tr class="js-old-images">
                    <td class="js_image">
                        <input type="hidden" name="image_old" value="<?= $texture->name ?>">
                        <img src="<?= $path_to_images . $texture->name ?>">
                    </td>
                    <td class="js_show"><input type="checkbox" <?= $texture->active === '0' ? '' : 'checked' ?>></td>
                    <td class="js_category">
                        <select name="category_id">
                            <option selected>Без категорії</option>
                            <?php foreach ($categories as $category): ?>
                                <option <?= $category->ID == $texture->category_id ? 'selected' : '' ?> value="<?= $category->ID ?>"><?= $category->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </td>
                    <td class="js_action"><button>x</button></td>
                </tr>
            <?php endforeach; ?>

        </tbody>
    </table>

    <button value="submit">Зберегти</button>
    <span class="save-done" id="save_done">Збережено!</span>
</section>

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/textures/main.js"></script>