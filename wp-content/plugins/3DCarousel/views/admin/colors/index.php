<style>
    table tbody tr td.js_image img {
        width: 100px;
        height: 100px;
    }

    .save-done {
        display: none;
    }
</style>

<section>
    <button id="add-row">+</button>
    <table id="gallery">
        <thead>
        <tr>
            <td>
                Колір
            </td>
            <td>
                Відображати
            </td>
            <td>
                Дія
            </td>
        </tr>
        </thead>
        <tbody>
        <tr style="display: none">
            <td class="js_color">
                <input type="color" name="colors">
            </td>
            <td class="js_show"><input type="checkbox"></td>
            <td class="js_action"><button>x</button></td>
        </tr>

        <?php foreach ($colors as $color): ?>
            <tr>
                <td class="js_color">
                    <input type="color" name="colors" value="<?= $color->name ?>">
                </td>
                <td class="js_show"><input type="checkbox" <?= $color->active === '0' ? '' : 'checked' ?>></td>
                <td class="js_action"><button>x</button></td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>

    <button value="submit">Зберегти</button>
    <span class="save-done" id="save_done">Збережено!</span>
</section>

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/colors/main.js"></script>