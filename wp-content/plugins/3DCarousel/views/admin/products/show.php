<!DOCTYPE html>
<html lang="en">

<head>
    <title>Carousel Plugin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?= CAROUSEL_3D_URL_ASSET ?>css/style.css">
</head>

<body>

<form action="<?= $uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="class" value="Products">
    <input type="hidden" name="method" value="<?= $method ?>">

    <div class="viewer-wrapper">
        <div class="container"></div>
        <div class="setting-wrapper">
            <div class="row-setting initial-content">
                <label for="mtl">Upload .mtl file
                    <input id="mtl" name="mtl" type="file">
                </label>

                <label for="object">Upload .obj file
                    <input id="object" name="obj" type="file">
                </label>

                <button id="start" type="button" name="button" disabled>Start</button>
            </div>
            <div class="row-setting">
                <label for="active">Активний
                    <input type="checkbox" name="active" id="active" <?= $product->active === '0' ? '' : 'checked' ?>>
                </label>
                <label for="name">Назва
                    <input type="text" name="name" id="name" value="<?= $product->product_name ?>">
                </label>
            </div>

            <div class="row-setting">
                <select name="category_id">
                    <option selected>Без категорії</option>
                    <?php if($categories): ?>
                        <?php foreach($categories as $category): ?>
                            <option value="<?= $category->ID ?>" <?= $product->category_id == $category->ID ? 'selected' : '' ?>><?= $category->name ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>

                <select name="collection_id">
                    <option selected>Без колекції</option>
                    <?php if($collections): ?>
                        <?php foreach ($collections as $collection): ?>
                            <option value="<?= $collection->ID ?>" <?= $product->collection_id == $collection->ID ? 'selected' : '' ?>><?= $collection->name ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>

            <!-- <div class="setting color-wrapper" id="color">
                <p>Choose color</p>
                <label for="color_1" style="background-color: #EABFB9">
                    <input name="color" id="color_1" type="radio" value="0xEABFB9">
                </label>
                <label for="color_2" style="background-color: #CBA08E">
                    <input name="color" id="color_2" type="radio" value="0xCBA08E">
                </label>
                <label for="color_3" style="background-color: #8B8589">
                    <input name="color" id="color_3" type="radio" value="0x8B8589">
                </label>
                <label for="color_4" style="background-color: #3a89b7">
                    <input name="color" id="color_4" type="radio" value="0x3a89b7">
                </label>
            </div> -->

            <!--            <div class="setting texture-wrapper" id="texture">-->
            <!--                <p>Choose texture</p>-->
            <!--                <div class="wood">-->
            <!--                    <span>Wood</span>-->
            <!--                </div>-->
            <!--                <div class="fabric">-->
            <!--                    <span>Fabric</span>-->
            <!--                    <label for="texture_1" style='background-image: url(./texture/leather.jpg)'>-->
            <!--                        <input name="texture" id="texture_1" type="radio" value="leather.jpg">-->
            <!--                    </label>-->
            <!--                    <label for="texture_2" style='background-image: url(./texture/leather2.jpg)'>-->
            <!--                        <input name="texture" id="texture_2" type="radio" value="leather2.jpg">-->
            <!--                    </label>-->
            <!--                    <label for="texture_3" style='background-image: url(./texture/leather3.jpg)'>-->
            <!--                        <input name="texture" id="texture_3" type="radio" value="leather3.jpg">-->
            <!--                    </label>-->
            <!--                </div>-->
            <!---->
            <!--            </div>-->

            <button type="button" class="btn js-btn-rotate">Turn off/on auto rotate</button>

            <!--        <button class="btn js-btn-shadow">Turn off/on shadow</button>-->
            <div class="row-setting">
                <label for="paddingX">Set padding X
                    <input id="paddingX" name="padding_x" type="number" value="<?= $product->padding_x ?: 1 ?>" min="1" max="1000">
                </label>

                <label for="paddingY">Set padding Y
                    <input id="paddingY" name="padding_y" type="number" value="<?= $product->padding_y ?: 1 ?>" min="1" max="1000">
                </label>

                <label for="shadow">Turn off/on shadow
                    <input type="checkbox" name="shadow" id="shadow" class="btn js-btn-shadow" <?= $product->shadow == 1 ? 'checked' : '' ?>>
                </label>
            </div>
            <div class="row-setting">
                <label for="zoom">Set zoom
                    <input id="zoom" name="zoom" type="number" value="<?= $product->zoom ?: 1 ?>" min="1" max="10000">
                </label>

                <label for="minZoom">Set min zoom
                    <input id="minZoom" name="min_zoom" type="number" value="<?= $product->min_zoom ?: 1 ?>" min="1" max="10000">
                </label>

                <label for="maxZoom">Set max zoom
                    <input id="maxZoom" name="max_zoom" type="number" value="<?= $product->max_zoom ?: 1000 ?>" min="1" max="10000">
                </label>
            </div>
        </div>
    </div>

    <p>All materials <span id="materials-counts">(6)</span></p>
    <ol class="materials-info">
        <li class="js-default-material default-material">
            <button type="button" name="materials">
                <input type="hidden" name="materials_alias[]" value="" readonly>
                <input type="text" name="materials_name[]" value="" readonly>
                <span class="js-material-edit">ред.</span>
            </button>

            <?php $material = $product->materials[$material_id]; ?>
            <?php require 'inc/textures.php' ?>
            <?php require 'inc/colors.php' ?>
        </li>

        <?php if(isset($product->materials) && is_array($product->materials)): ?>
            <?php foreach($product->materials as $material_id => $material): ?>
                <li class="js-default-material">
                    <button type="button" name="materials">
                        <input type="hidden" name="materials_alias[]" value="<?= $material['alias'] ?>" readonly>
                        <input type="text" name="materials_name[]" value="<?= $material['name'] ?>" readonly>
                        <span class="js-material-edit">ред.</span>
                    </button>

                    <?php $material = $product->materials[$material_id]; ?>
                    <?php require 'inc/textures.php' ?>
                    <?php require 'inc/colors.php' ?>
                </li>
            <?php endforeach; ?>
        <?php endif; ?>
    </ol>

    <button type="submit">Зберегти</button>
</form>

<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/libs/three.min.js"></script>

<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/libs/OrbitControls.js"></script>
<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/libs/WebGL.js"></script>

<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/libs/DDSLoader.js"></script>
<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/libs/MTLLoader.js"></script>
<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/libs/OBJLoader.js"></script>

<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/products/scene.js"></script>
<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/products/object.js"></script>
<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/products/color.js"></script>
<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/products/plugin.js"></script>

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script>
    // $(document).ready(filterCategory); 
    // $(document).on('change', 'select[name="category_id"]', filterCategory);

    // Filter textures having category_id active field.
    // function filterCategory() {
    //     $('.js-default-material').each(function () {
    //         // Get id active category.
    //         const selectedCategoryId = $(this).find('select[name="category_id"] option:selected').val();
    //         console.log(selectedCategoryId)

    //         // Hide textures not active category.
    //         $(this).find('li[data-category_id!="' + selectedCategoryId + '"]').each(function () {
    //             $(this).fadeOut('fast');
    //         });

    //         // Show textures active category.
    //         $(this).find('li[data-category_id="' + selectedCategoryId + '"]').each(function () {
    //             $(this).fadeIn('fast');
    //         });
    //     })
    // }

    let container, stats;
    let object;
    let camera, scene, controls;

    let mouse = new THREE.Vector2(), INTERSECTED;

    let activeMaterial, activeMaterialName;

    let dirLightShadowMapViewer, spotLightShadowMapViewer;

    let dirLight, spotLight;

    let canvas, bounds;

    let lastIndex;

    let mtlData, objectData;

    let zoom, minZoom, maxZoom;

    let modelExist = false;
    let updateMtlEvent, updateObjectEvent;

    let lastObject;

    let defaultColor = 0x7a7a7a;


    <?php if($product->mtl): ?>
        mtlData = '<?= CAROUSEL_3D_PRODUCTS_URL_GALLERY . $product->mtl ?>';
    <?php endif; ?>

    <?php if($product->obj): ?>
        objectData = '<?= CAROUSEL_3D_PRODUCTS_URL_GALLERY . $product->obj ?>';
    <?php endif; ?>

    <?php if($product->obj && $product->mtl): ?>
        modelExist = true;
        document.addEventListener('DOMContentLoaded', initilaisationModel, false);
        document.getElementById('start').addEventListener('click', updateModel, false);

        clickOnMaterials();

    <?php endif; ?>

    let maxDistance, rotateSpeed;

    const pathToTextures = '<?= $path_to_textures ?>';

    document.getElementById('start').addEventListener('click', initilaisationModel, false);

    // document.querySelector('.materials-info').addEventListener('click', clickOnMaterials, false);

    init();
    animate();

    // document.addEventListener('click', clickOnMaterials, false);


    function initilaisationModel () {

        if(!mtlData || !objectData) {
            return alert("Select files")
        }

        document.getElementById('start').setAttribute('disabled', true);
        console.log(modelExist)

      

        // setTimeout(() => {
        // // Delete previous model before downloading new.
        //     if (!modelExist) {
        //     lastIndex = scene.children.length - 1;
        //     console.log(scene.children);
        //     scene.children.splice(lastIndex, 1);
        // }}, 300)

        initObject();

        // for(let i=0; i<scene.children.length; i++) {
        // }

    }

    // function completeDownloading() {
    //     if(scene.children.length < 6) {
    //         console.log(scene.children)
    //         completeDownloading();
    //     }

    //     console.log(scene.children[5])
    // }


    function updateModel() {
        if(modelExist) {
            console.log(1)
            document.getElementById('start').setAttribute('disabled', true);
            lastIndex = scene.children.length - 1;
            scene.children.splice(lastIndex, 1);
            // initObject();
        }
    }

    document.getElementById('mtl').addEventListener('change', function (e) {
        let file = this.files[0];
        mtlData = window.URL.createObjectURL(file);

        updateMtlEvent = true;
        updatePreviewButton()
    });

    document.getElementById('object').addEventListener('change', function (e) {
        let file = this.files[0];
        objectData = window.URL.createObjectURL(file);

        updateObjectEvent = true;
        updatePreviewButton();
    });

    document.getElementById('maxZoom').addEventListener('keyup', function(e){
        e.preventDefault();

        maxZoom = this.value;
        controls.maxDistance = this.value;
        controls.update();
    });

    document.getElementById('minZoom').addEventListener('keyup', function(e){
        e.preventDefault();

        minZoom = this.value
        controls.minDistance = this.value;
        controls.update();
        // camera.zoom = this.value;
    });

    document.getElementById('zoom').addEventListener('keyup', function(e){
        e.preventDefault();

        zoom = this.value;
        camera.position.z = this.value;
        controls.update();
    });


    // Click on materials.
    function clickOnMaterials() {
        let materialsItem = document.querySelectorAll('.js-default-material:not(.default-material)');

        let newMaterialsItem = document.querySelectorAll('.js-default-material[hidden="false"]');

        let materials;


        if (!modelExist) {
            materials = newMaterialsItem;
        } else {
            materials = materialsItem;
        }

        Array.from(materials).forEach(item => {

           item.addEventListener('click', function(e){
              let targets = document.querySelectorAll('.active-material');
              let focusMaterialName = this.querySelector('input[type="hidden"]').getAttribute('value');
              lastIndex = scene.children.length - 1;
              let object = scene.children[lastIndex];

               object.traverse(function(child){
                   if (child instanceof THREE.Mesh) {
                       if(child.material.name === focusMaterialName) {
                            child.material.color.setHex(0xff0000);

                            for(let i = 0; i < targets.length; i++) {
                              targets[i].classList.remove('active-material');
                            }

                            item.classList.add('active-material');

                       } else {
                            child.material.color.setHex(defaultColor);
                       }
                   }
               });
            });
        });

        document.addEventListener('click', function(e){
            let attr = materials[0].classList[0];

            lastIndex = scene.children.length - 1;
            let object = scene.children[lastIndex];

            if (!(e.target).closest(`.${attr}`) && !(e.target).closest('canvas')) {
                object.traverse(function(child){
                    if (child instanceof THREE.Mesh) {
                        child.material.color.setHex(defaultColor);
                    }
                });

                let array = document.querySelectorAll('.materials-info li.js-default-material');
                for (i = 0; i < array.length; i++){
                    array[i].classList.remove('active-material');
                };
            };
        })
    }

    function updatePreviewButton () {
        if(updateMtlEvent && updateObjectEvent) {
            document.getElementById('start').removeAttribute('disabled');
        }
    }

  //   function myFunction(e) {
  //     var elems = document.querySelectorAll(".active-material");
  //     [].forEach.call(elems, function(el) {
  //       el.classList.remove("active-material");
  //     });
  //     // console.log(e.target.className)
  //     e.target.classList.add('active-material')
  // }







</script>

</body>

</html>

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/products/main.js"></script>
