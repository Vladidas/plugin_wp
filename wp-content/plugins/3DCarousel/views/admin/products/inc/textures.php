<select name="category_id" >
    <option selected>Без категорії</option>
    <?php if ($texture_categories): ?>
        <?php foreach ($texture_categories as $category): ?>
            <option><?= $category->name ?></option>
        <?php endforeach; ?>
    <?php endif; ?>
</select>

<ul class="textures">
    <?php if($textures): ?>
        <?php foreach($textures as $key => $texture): ?>
            <?php $textures_id = !empty($material['textures']) ? json_decode($material['textures']) : []; ?>
            <?php $is_checked = in_array($texture->ID, $textures_id); ?>
            <li data-category_id="<?= $texture->category_id ?>">
                <label for="texture_<?= $key ?>">
                    <input type="checkbox" name="textures[<?= $material_id ?>][]" id="texture_<?= $key ?>" value="<?= $texture->ID ?>" <?= $is_checked ? 'checked' : '' ?>>
                </label>
                <img value="<?= $texture->name ?>" src="<?= $path_to_textures . $texture->name ?>">
            </li>
        <?php endforeach; ?>
    <?php else: ?>
        <li>Текстур немає!</li>
    <?php endif; ?>
</ul>