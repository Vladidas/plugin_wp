<ul class="colors">
    <?php if($colors): ?>
        <?php foreach($colors as $key => $color): ?>
            <?php $colors_id = !empty($material['colors']) ? json_decode($material['colors']) : []; ?>
            <?php $is_checked = in_array($color->ID, $colors_id); ?>
            <li>
                <label for="color_<?= $key ?>" style="background-color: <?= $color->name ?>"><?= $color->name ?></label>
                <input type="checkbox" name="colors[<?= $material_id ?>][]" id="color_<?= $key ?>" value="<?= $color->ID ?>" <?= $is_checked ? 'checked' : '' ?>>
            </li>
        <?php endforeach; ?>
    <?php else: ?>
        <li>Кольорів немає!</li>
    <?php endif; ?>
</ul>