<section>
    <button><a href="<?= $uri . '&method=create' ?>">Додати новий</a></button>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Назва</th>
                <th>MTL</th>
                <th>OBJ</th>
                <th>Активний</th>
                <th>Дія</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($products as $product): ?>
                <tr>
                    <td><?= $product->ID ?: '-' ?></td>
                    <td><?= $product->name ?: '-' ?></td>
                    <td><?= $product->mtl ?: '-' ?></td>
                    <td><?= $product->obj ?: '-' ?></td>
                    <td>
                        <?php if($product->active == 1): ?>
                            +
                        <?php else: ?>
                            -
                        <?php endif; ?>
                    </td>
                    <td class="js-delete"><button><a href="<?= $uri . '&method=delete&ID=' . $product->ID ?>">вид</a></button></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</section>

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script>
    // Set active material name status.
    $(document).on('click', '.js-material-edit', function() {
        $(this).siblings('input[type="text"]').removeAttr('readonly');
    });

    // Set all button without active readonly status.
    $(document).on('click', function(e) {
        if(!$(e.target).is('button[name="materials"]') || !$(e.target).is('button[name="materials"] *')) {
            $('button[name="materials"]').not($(e.target).closest('button[name="materials"]')).find('input[type="text"]').attr('readonly', true);
        }
    });

    // Open product.
    $(document).on('click', 'table tbody tr', function(e) {
        const id = $(this).find('td:first').text();
        location.href = '<?= $uri . '&method=show' ?>&ID=' + id;
    });
</script>