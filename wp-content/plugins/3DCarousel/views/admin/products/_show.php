<style>
    input:read-only {
        background: grey;
    }

    .textures img {
        width: 100px;
        height: 100px;
    }

    /*.material * {*/
        /*height: 100px;*/
    /*}*/

    .preview {
        height: 500px;
        width: 300px;
        border: 1px solid black;
    }
</style>

<section>
    <form action="<?= $uri ?>" method="post">
        <input type="hidden" name="class" value="Products">
        <input type="hidden" name="method" value="<?= $method ?>">

        <label for="obj">Макет(OBJ-файл)</label>
        <input type="file" name="obj" id="obj">

        <label for="mtl">Опис макету(MTL-файл)</label>
        <input type="file" name="mtl" id="mtl">

        <div class="preview" id="preview">

        </div>

        <label for="active">Активний</label>
        <input type="checkbox" name="active" id="active" <?= $product->active == 1 ? 'checked' : '' ?>>

        <label for="name">Назва</label>
        <input type="text" name="name" id="name" value="<?= $product->name ?: '' ?>">

        <select name="category_id">
            <option selected>Виберіть категорію</option>
            <?php if($categories): ?>
                <?php foreach($categories as $category): ?>
                    <option value="<?= $category->ID ?>" <?= $product->category_id == $category->ID ? 'selected' : '' ?>><?= $category->name ?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>

        <select name="collection_id">
            <option selected>Виберіть колекцію</option>
            <?php if($collections): ?>
                <?php foreach ($collections as $collection): ?>
                    <option value="<?= $collection->ID ?>" <?= $product->collection_id == $collection->ID ? 'selected' : '' ?>><?= $collection->name ?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>

        <label for="padding_x">Відступ по Х</label>
        <input type="number" name="padding_x" id="padding_x" value="<?= $product->padding_x ?: 0 ?>" min="0" max="1000">

        <label for="padding_y">Відступ по Y</label>
        <input type="number" name="padding_y" id="padding_y" value="<?= $product->padding_y ?: 0 ?>" min="0" max="1000">

        <label for="zoom">Масштаб</label>
        <input type="number" name="zoom" id="zoom" min="0" value="<?= $product->zoom ?: 0 ?>" max="1000">

        <label for="min_zoom">Мінімальний масштаб</label>
        <input type="number" name="min_zoom" id="min_zoom" value="<?= $product->zoom ?: 0 ?>" min="0" max="1000">

        <label for="max_zoom">Максимальний масштаб</label>
        <input type="number" name="max_zoom" id="max_zoom" value="<?= $product->max_zoom ?: 0 ?>" min="0" max="1000">

        <label for="speed_rotate">Швидкість обертання</label>
        <input type="number" name="speed_rotate" id="speed_rotate" value="<?= $product->speed_rotate ?: 0 ?>" min="0" max="1000">

        <label for="shadow">Тіні</label>
        <input type="checkbox" name="shadow" id="shadow" <?= $product->active == 1 ? 'checked' : '' ?>>

        <ul class="material">
            <?php if(isset($product->materials) && is_array($product->materials)): ?>
                <?php foreach($product->materials as $material_id => $material): ?>
                    <li>
                        <button type="button" name="materials">
                            <input type="text" name="materials[<?= $material_id ?>]" value="<?= $material['name']?>" readonly>
                            <span class="js-material-edit">ред.</span>
                        </button>

                        <?php $material = $product->materials[$material_id]; ?>
                        <?php require 'inc/textures.php' ?>
                        <?php require 'inc/colors.php' ?>

                    </li>
                <?php endforeach; ?>
            <?php else: ?>
                <li>Матеріалів немає!</li>
            <?php endif; ?>
        </ul>

        <button type="submit">Зберегти</button>
    </form>
</section>

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/products/main.js"></script>