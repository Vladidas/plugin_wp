<?php

namespace user;

class Carousel extends \BasicController
{
    static $db;

    /** Load DB as @singleton. */
    public function DB()
    {
        return self::$db = self::$db ?: new \DB();
    }

    /** Get product with textures and colors info to array. */
    static function show($args = null)
    {
        $product = null;

        if(isset($args['id']) && !empty($args['id'])) {
            // Get product with materials.
            $product = self::getProductWithMaterials($args['id']);

            if($product) {
                // Get textures and colors from DB.
                $textures = self::getTextures(true);
                $colors = self::getColors(true);

                // Change product materials to show multidimensional array.
                if(is_array($product->materials)) {
                    foreach($product->materials as $material_id => $material) {
                        // Change textures.
                        if($textures) {
                            $textures_id = json_decode($product->materials[$material_id]['textures']);
                            $product->materials[$material_id]['textures'] = [];
                            foreach($textures as $texture) {
                                if(in_array($texture->ID, $textures_id)) {
                                    $product->materials[$material_id]['textures'][] = $texture;
                                }
                            }
                        }

                        // Change colors.
                        if($colors) {
                            $colors_id = json_decode($product->materials[$material_id]['colors']);
                            $product->materials[$material_id]['colors'] = [];
                            foreach($colors as $color) {
                                if(in_array($color->ID, $colors_id)) {
                                    $product->materials[$material_id]['colors'][] = $color;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $product;
    }

    /** Get product and materials with textures and colors. */
    private function getProductWithMaterials($product_id)
    {
        $products = new \Products();

        return $products->getProductWithMaterials($product_id);
    }

    /** Get textures to array. */
    static function getTextures()
    {
        $db = self::DB();
        $query = 'SELECT * FROM ' . $db->prefix . CAROUSEL_3D_TEXTURES_TBL_NAME . ' WHERE active = 1';

        return $db->selectAll($query);
    }

    /** Get colors to array. */
    static function getColors()
    {
        $db = self::DB();
        $query = 'SELECT * FROM ' . $db->prefix . CAROUSEL_3D_COLORS_TBL_NAME . ' WHERE active = 1';

        return $db->selectAll($query);
    }
}