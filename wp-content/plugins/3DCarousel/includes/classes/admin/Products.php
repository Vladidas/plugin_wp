<?php

class Products extends BasicController
{
    public function route()
    {
        return parent::route();
    }

    /** Show all products. */
    public function index()
    {
        $uri = parent::getCurrentUri();

        $db = new DB();

        $products = $db->selectAll('SELECT * FROM ' . $db->prefix . CAROUSEL_3D_PRODUCTS_TBL_NAME);

        return parent::view('admin/products/index.php', compact('products', 'uri'));
    }

    /** Show product. */
    public function show($request)
    {
        $id = $request['ID'] ?: null;
        $uri = parent::getCurrentUri();
        $path_to_textures = CAROUSEL_3D_TEXTURES_URL_GALLERY;
        $path_to_products = CAROUSEL_3D_PRODUCTS_DIR_GALLERY;
        $method = 'update';

        if(!$id) {
            return $this->index();
        }

        $product = $this->getProductWithMaterials($id);
        $textures = (new Textures())->getAll();
        $colors = (new Colors())->getAll();
        $categories = (new ProductCategories())->getAll();
        $collections = (new ProductCollections())->getAll();
        $texture_categories = (new TextureCategories())->getAll();

        return parent::view('admin/products/show.php', compact('product', 'categories', 'texture_categories', 'path_to_products', 'collections', 'colors', 'textures', 'uri', 'method', 'path_to_textures'));
    }

    /** Get product and materials with textures and colors. */
    public function getProductWithMaterials($product_id)
    {
        // Get materials with product.
        $db = new DB();

        // Get tables name.
        $table_product = $db->prefix . CAROUSEL_3D_PRODUCTS_TBL_NAME;
        $table_product_materials = $db->prefix . CAROUSEL_3D_PRODUCT_MATERIALS_TBL_NAME;

        // Get hasMany relationship.
        $product_id = $db->wpdb->prepare('%d ', $product_id);
        $materials = $db->selectAll("SELECT A.*, A.name AS product_name, B.* FROM $table_product_materials AS B JOIN $table_product AS A ON A.ID = B.product_id AND A.ID = $product_id");

        // Build result hasMany array.
        $product = null;
        if (!empty($materials)) { // Get product with relationship if exits.
            $product = $materials[0];
            foreach ($materials as $material) {
                $product->materials[$material->ID]['name'] = $material->name;
                $product->materials[$material->ID]['alias'] = $material->alias;
                $product->materials[$material->ID]['colors'] = $material->colors;
                $product->materials[$material->ID]['textures'] = $material->textures;
            }
        } else { // Get only product if relationship isn't exist.
            $product = $db->select("SELECT * FROM $table_product WHERE ID = $product_id");
        }

        return $product;
    }

    /** Create product. */
    public function create()
    {
        $uri = parent::getCurrentUri();
        $path_to_textures = CAROUSEL_3D_TEXTURES_URL_GALLERY;
        $path_to_products = CAROUSEL_3D_PRODUCTS_DIR_GALLERY;
        $method = 'save';

        $textures = (new Textures())->getAll();
        $colors = (new Colors())->getAll();
        $categories = (new ProductCategories())->getAll();
        $collections = (new ProductCollections())->getAll();
        $texture_categories = (new TextureCategories())->getAll();

        return parent::view('admin/products/show.php', compact('uri', 'categories', 'texture_categories', 'path_to_products', 'collections','method', 'colors', 'textures', 'path_to_textures'));
    }

    /** Save product. */
    public function save($request)
    {
        // Build result value for the update from DB.
        $values = [
            'name'         => $request['name'],
            'mtl'          => $request['mtl'],
            'obj'          => $request['obj'],
            'padding_x'    => (int) $request['padding_x'],
            'padding_y'    => (int) $request['padding_y'],
            'zoom'         => (int) $request['zoom'],
            'min_zoom'     => (int) $request['min_zoom'],
            'max_zoom'     => (int) $request['max_zoom'],
            'speed_rotate' => (int) $request['speed_rotate'],
            'shadow'       => (int) isset($request['shadow']),
            'active'       => (int) isset($request['active']),
        ];

        // If exist file mtl - attach to update from DB.
        if(!empty($_FILES['mtl'])) {
            if($file = $this->saveFile($_FILES, 'mtl')) {
                $values = array_merge($values, $file);
            }
        }

        // If exist file obj - attach to update from DB.
        if(!empty($_FILES['obj'])) {
            if($file = $this->saveFile($_FILES, 'obj')) {
                $values = array_merge($values, $file);
            }
        }

        // Save product to DB.
        $db = new \DB();
        $product_id = $db->insert(CAROUSEL_3D_PRODUCTS_TBL_NAME, $values, [
            '%s', '%s', '%s', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d'
        ]);

        // Save product materials.
        $this->saveProductMaterials($request, $product_id);

        // Redirect to index.
        parent::redirect(parent::getCurrentUri(true, '?page=Products'));
    }

    /** Update product. */
    public function update($request)
    {
        $id = $_GET['ID'] ?: null;

        if($id) {
            // Update product from DB.
            $db = new \DB();

            // Build result value for the update from DB.
            $values = [
                'name'          => $request['name'],
                'category_id'   => $request['category_id'],
                'collection_id' => $request['collection_id'],
                'padding_x'     => (int) $request['padding_x'],
                'padding_y'     => (int) $request['padding_y'],
                'zoom'          => (int) $request['zoom'],
                'min_zoom'      => (int) $request['min_zoom'],
                'max_zoom'      => (int) $request['max_zoom'],
                'speed_rotate'  => (int) $request['speed_rotate'],
                'shadow'        => (int) isset($request['shadow']),
                'active'        => (int) isset($request['active']),
            ];

            // If exist file mtl - attach to update from DB.
            if(!empty($_FILES['mtl'])) {
                if($file = $this->saveFile($_FILES, 'mtl')) {
                    $values = array_merge($values, $file);
                }
            }

            // If exist file obj - attach to update from DB.
            if(!empty($_FILES['obj'])) {
                if($file = $this->saveFile($_FILES, 'obj')) {
                    $values = array_merge($values, $file);
                }
            }

            // Update product data.
            $db->update(CAROUSEL_3D_PRODUCTS_TBL_NAME, $values, ['ID' => $id]);

            // Save product materials.
            $this->saveProductMaterials($request, $id);
        }

        // Redirect to index.
        parent::redirect(parent::getCurrentUri(true, '?page=Products'));
    }

    /** Save file for the product. */
    private function saveFile($file, $type)
    {
        $file = $_FILES[$type];

        if(strtolower(parent::getFileExt($file['name'])) === $type)  {
            $name = parent::getNewFileName($file['name']);

            parent::moveUploadFile($file['tmp_name'], CAROUSEL_3D_PRODUCTS_DIR_GALLERY . $name);

            return [$type => $name];
        }

        return false;
    }

    /** Save materials for product relationship. */
    private function saveProductMaterials($request, $product_id)
    {
        // Clear default value,
        unset($request['materials_alias'][0], $request['materials_name'][0], $request['textures'][0], $request['colors'][0]);

        // Update product from DB.
        $db = new \DB();
        $db->delete(CAROUSEL_3D_PRODUCT_MATERIALS_TBL_NAME, ['product_id' => $product_id]);

        if($request['materials_name']) {
            foreach ($request['materials_name'] as $material_id => $material_name) {
                $db->insert(CAROUSEL_3D_PRODUCT_MATERIALS_TBL_NAME, [
                    'product_id' => $product_id,
                    'name' => $material_name,
                    'alias' => $request['materials_alias'][$material_id] ?: '',
                    'textures' => json_encode($request['textures'][$material_id] ?: []),
                    'colors' => json_encode($request['colors'][$material_id] ?: []),
                ], [
                    '%d', '%s', '%s', '%s'
                ]);
            }
        }
    }

    /** Delete product. */
    public function delete($request)
    {
        $id = $request['ID'] ?: null;

        if($id) {
            $db = new \DB();
            $db->delete(CAROUSEL_3D_PRODUCTS_TBL_NAME, ['ID' => $id]);
        }

        // Redirect to index.
        parent::redirect(parent::getCurrentUri(true, '?page=Products'));
    }
}