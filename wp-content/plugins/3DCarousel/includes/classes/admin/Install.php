<?php

class Install
{
    static $path_to_gallery = ABSPATH . 'wp-content/uploads/3DCarousel';

    private $admin_menu;

    public function __construct()
    {
        $this->admin_menu = $GLOBALS['admin_menu'];
    }

    /** Activate plugin. */
    public static function activate()
    {
        // Create tables in DB.
        $db = (new self)->getDB();
        $db->createTexturesTable();
        $db->createTextureCategoriesTable();
        $db->createColorsTable();
        $db->createProductsTable();
        $db->createProductMaterialsTable();
        $db->createProductCategoriesTable();
        $db->createProductCollectionsTable();

        // Create dir and set chmod 777.
        if(!file_exists(self::$path_to_gallery)) {
            mkdir(self::$path_to_gallery);
            chmod(self::$path_to_gallery, 0777);

            // Create textures folder for gallery.
            if(!file_exists(self::$path_to_gallery . '/textures/')) {
                mkdir(self::$path_to_gallery . '/textures', 0777);
                chmod(self::$path_to_gallery . '/textures', 0777);
            }

            // Create products folder for gallery.
            if(!file_exists(self::$path_to_gallery . '/products/')) {
                mkdir(self::$path_to_gallery . '/products', 0777);
                chmod(self::$path_to_gallery . '/products', 0777);
            }

            // Copy default texture to gallery folder.
            if(file_exists(CAROUSEL_3D_DIR_ASSET . 'img/default_texture.jpg')) {
                copy(CAROUSEL_3D_DIR_ASSET . 'img/default_texture.jpg', self::$path_to_gallery . '/textures/default_texture.jpg');
            }
        }
    }

    /** Deactivate plugin. */
    public static function deactivate()
    {
        self::uninstall();
        return true;
    }

    /** Deactivate plugin. */
    public static function uninstall()
    {
        // Remove DB.
        $db = (new self)->getDB();
        $db->deleteTexturesTable();
        $db->deleteTextureCategoriesTable();
        $db->deleteColorsTable();
        $db->deleteProductsTable();
        $db->deleteProductMaterialsTable();
        $db->deleteProductCategoriesTable();
        $db->deleteProductCollectionsTable();

        // Remove dir.
        $controller = new BasicController();
        $controller->removeDir(self::$path_to_gallery );
    }

    /** Get DB for the operation. */
    private function getDB()
    {
        return new DB();
    }

    /** Add plugin to Admin menu. */
    public function buildAdminMenu()
    {
        // Hook add to admin menu.
        add_action('admin_menu', function () {
            // Add parent menu item.
            add_menu_page($this->admin_menu['name'], $this->admin_menu['name'], 'manage_options', $this->admin_menu['name'], [$this->admin_menu['class'], $this->admin_menu['method']]);

            // Add child menu items.
            if (function_exists('add_menu_page')) {
                foreach ($this->admin_menu['child'] as $child_menu) {
                    add_submenu_page($this->admin_menu['name'], $child_menu['name'],  $child_menu['name'], 'manage_options', $child_menu['class'], [$child_menu['class'], $child_menu['method']]);
                }
            }
        });
    }
}