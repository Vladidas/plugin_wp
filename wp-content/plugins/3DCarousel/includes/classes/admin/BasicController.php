<?php

class BasicController
{
    /** Show HTML to view. */
    public function view($file_name, $args)
    {
        if(!file_exists(CAROUSEL_3D_ADMIN_VIEW_FOLDER . $file_name)) {
            return die('Файл (' . CAROUSEL_3D_ADMIN_VIEW_FOLDER . $file_name . ') не найдений!');
        }

        extract($args);

        return require_once CAROUSEL_3D_ADMIN_VIEW_FOLDER . $file_name;
    }

    /** Renderer view HTML to variable. */
    public function viewRender($file_name, $args)
    {
        ob_start();

        $this->view($file_name, $args);

        return ob_get_contents();
    }

    /** Routing request and get action. */
    public function route()
    {
        if(isset($_GET['page']) && isset($_GET['method'])) {
            require_all();

            $request = $_GET;
            $method = $request['method'] ?: null;

            if($method) {
                return (new static())->$method($request);
            }
        }

        return (new static())->index();
    }

    /** Remove all files in directory and directory. */
    public function removeDir($dirname)
    {
        if (is_dir($dirname)) {
            $dir = new RecursiveDirectoryIterator($dirname, RecursiveDirectoryIterator::SKIP_DOTS);
            foreach (new RecursiveIteratorIterator($dir, RecursiveIteratorIterator::CHILD_FIRST ) as $filename => $file) {
                if (is_file($filename))
                    unlink($filename);
                else
                    rmdir($filename);
            }
            rmdir($dirname);
        }
    }

    /** Get current page URI with GET-arguments. */
    public function getCurrentUri($without_args = null, $concat_uri = null)
    {
        if(!$without_args) {
            return $_SERVER['REQUEST_URI'];
        }

        return home_url() . $_SERVER['PHP_SELF'] . $concat_uri;
    }

    /** Redirects to another page. */
    public function redirect($url)
    {
        header("Location: " . $url);
        return die;
    }

    /** Generate random for the validation string use as filename. */
    public function randString($length = 15)
    {
        return substr(md5(mt_rand()), 0, $length);
    }

    /** Get file extension. */
    public function getFileExt($name)
    {
        return pathinfo($name, PATHINFO_EXTENSION);
    }

    /** Get filename with extensiion.  */
    public function getNewFileName($name)
    {
        return $this->randString(15) . '.' . $this->getFileExt($name);
    }

    /** Move upload file to dir.  */
    public function moveUploadFile($tmp_name, $path_with_name)
    {
        return move_uploaded_file($tmp_name, $path_with_name);
    }
}