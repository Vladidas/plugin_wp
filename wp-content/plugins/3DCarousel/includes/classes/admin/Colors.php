<?php

class Colors extends BasicController
{
    public function index()
    {
        $colors = self::getAll();

        return parent::view('admin/colors/index.php', compact('colors'));
    }

    /** Get all colors. */
    public function getAll($only_active = false)
    {
        $db = new DB();

        $active_concat = '';
        if($only_active) {
            $active_concat = ' WHERE active = 1';
        }

        return $db->selectAll('SELECT * FROM ' . $db->prefix . CAROUSEL_3D_COLORS_TBL_NAME . $active_concat);
    }

    /** Save post texture.. */
    public function save($request)
    {
        $colors = $request['colors'];

        $db = new DB();

        // Clear all fields.
        $db->query("DELETE FROM " . $db->prefix . CAROUSEL_3D_COLORS_TBL_NAME);

        // Insert new values.
        foreach($colors as $color) {
            $db->insert(CAROUSEL_3D_COLORS_TBL_NAME, [
                'name' => $color['name'] ?: null,
                'active' => $color['active'] === 'true',
            ], ['%s', '%d']);
        }

        echo true;
    }
}