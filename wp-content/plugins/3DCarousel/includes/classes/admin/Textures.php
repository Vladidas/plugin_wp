<?php

class Textures extends BasicController
{
    public function index()
    {
        $path_to_images = CAROUSEL_3D_TEXTURES_URL_GALLERY;
        $textures = self::getAll();
        $categories = (new TextureCategories)->getAll();

        return parent::view('admin/textures/index.php', compact('textures', 'categories', 'path_to_images'));
    }

    /** Get all textures. */
    public function getAll($only_active = false)
    {
        $db = new DB();

        $active_concat = '';
        if($only_active) {
            $active_concat = ' WHERE active = 1';
        }

        return $db->selectAll('SELECT * FROM ' . $db->prefix . CAROUSEL_3D_TEXTURES_TBL_NAME . $active_concat);
    }

    /** Save post texture.. */
    public function save($request)
    {
        $db = new DB();

        // Save old images.
        $this->removeOldImages($request, $db);

        // Update exists images.
        if(isset($request['old_images_active']) && !empty($request['old_images_active'])) {
            $this->updateExistImages($request, $db);
        }

        // Save new images.
        if(isset($_FILES['images']) && !empty($_FILES['images'])) {
            $this->saveNewImages($request, $db);
        }

        echo $this->syncImages($db);
    }

    /** Prepare image for the save. */
    private function prepareImage($files, $i)
    {
        return [
            'name' => $files['name'][$i],
            'type' => $files['type'][$i],
            'tmp_name' => $files['tmp_name'][$i],
            'size' => $files['size'][$i],
            'error' => $files['error'][$i],
        ];
    }

    /** Save images. */
    private function saveImage($image)
    {
        $name = parent::getNewFileName($image['name']);

        if($image['tmp_name']) {
            parent::moveUploadFile($image['tmp_name'], CAROUSEL_3D_TEXTURES_DIR_GALLERY . $name);
        }

        return $name;
    }

    /** Remove old images which remove form list. */
    private function removeOldImages($request, $db)
    {
        $names = '';
        if(isset($request['old_images_name']) && !empty($request['old_images_name'])) { // If not all images.
            foreach($request['old_images_name'] as $image) {
                $names .= "'" . $image . "',";
            }

            // Remove old images from DB.
            $names = substr($names, 0, -1);
            $db->query("DELETE FROM " . $db->prefix . CAROUSEL_3D_TEXTURES_TBL_NAME ." WHERE name NOT IN($names)");
        } else {
            $db->query("DELETE FROM " . $db->prefix . CAROUSEL_3D_TEXTURES_TBL_NAME);
        }
    }

    /** Update exist images which has is list. */
    private function updateExistImages($request, $db)
    {
        foreach($request['old_images_active'] as $key => $image) {
            $name = $request['old_images_name'][$key];
            $category_id = $request['old_images_category_id'][$key];

            $db->update(CAROUSEL_3D_TEXTURES_TBL_NAME, [
                'name' => $name ?: null,
                'category_id' => $category_id ?: null,
                'active' => ($image === 'true'),
            ], [
                'name' => $name
            ]);
        }
    }

    /** Save new uploads images. */
    private function saveNewImages($request, $db)
    {
        $files = $_FILES['images'];

        // Save new images
        for($i = 0; $i <= count($files['name']); $i++) {

            if($files['name'][$i]) {
                $img_name = $this->saveImage($this->prepareImage($files, $i));
                $active =  $request['active'][$i] === 'true';
                $category_id =  $request['category_id'][$i];

                $db->insert(CAROUSEL_3D_TEXTURES_TBL_NAME, [
                    'name' => $img_name,
                    'category_id' => $category_id,
                    'active' => $active
                ], ['%s', '%d']);
            }
        }
    }

    /** Remove all files in folder except exists files in DB. */
    private function syncImages($db)
    {
        // Get all images from DB for delete except include default texture.
        $leave_files = ['default_texture.jpg'];
        $textures = $db->selectAll('SELECT name FROM ' . $db->prefix . CAROUSEL_3D_TEXTURES_TBL_NAME);
        foreach($textures as $file) {
            $leave_files[] = $file->name;
        }

        // Remove all images except files from DB.
        $files = glob(CAROUSEL_3D_TEXTURES_DIR_GALLERY . '/*');
        foreach($files as $file){
            if(!in_array(basename($file), $leave_files)) {
                if(file_exists($file)) {
                    unlink($file);
                }
            }
        }

        return true;
    }
}