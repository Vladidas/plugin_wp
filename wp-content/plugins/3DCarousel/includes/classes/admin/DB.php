<?php

class DB
{
    private $charset;
    private $collate;
    public $prefix;

    public function __construct()
    {
        global $wpdb;

        $this->wpdb = $wpdb;
        $this->charset = $wpdb->charset;
        $this->collate = $wpdb->collate;
        $this->prefix = $wpdb->prefix;
    }

    /** Create table in DB. */
    private function createTable($table, $query)
    {
        // Check if exist table in DB.
        if($this->wpdb->get_var("show tables like '" . $table . "'") != $table) {
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            return dbDelta($query);
        }

        return false;
    }

    /** Delete table in DB. */
    private function deleteTable($table)
    {
        return $this->wpdb->query("DROP TABLE IF EXISTS " . $table);
    }

    /** Create textures table. */
    public function createTexturesTable()
    {
        // Get table name.
        $table = $this->prefix . CAROUSEL_3D_TEXTURES_TBL_NAME;

        // Build query.
        $query = "CREATE TABLE `" . $table . "` (
            `ID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `category_id` INT(10) NULL,
            `name` VARCHAR(50) NULL,
            `active` BOOLEAN,
            PRIMARY KEY (`ID`)
        ) COLLATE " . $this->collate . ";";

        // Create table.
        return $this->createTable($table, $query);
    }

    /** Delete textures table. */
    public function deleteTexturesTable()
    {
        // Get table name.
        $table = $this->prefix . CAROUSEL_3D_TEXTURES_TBL_NAME;

        // Delete table.
        return $this->deleteTable($table);
    }

    /** Create textures table. */
    public function createTextureCategoriesTable()
    {
        // Get table name.
        $table = $this->prefix . CAROUSEL_3D_TEXTURES_CATEGORIES_TBL_NAME;

        // Build query.
        $query = "CREATE TABLE `" . $table . "` (
             `ID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(50) NULL,
            PRIMARY KEY (`ID`)
        ) COLLATE " . $this->collate . ";";

        // Create table.
        return $this->createTable($table, $query);
    }

    /** Delete textures table. */
    public function deleteTextureCategoriesTable()
    {
        // Get table name.
        $table = $this->prefix . CAROUSEL_3D_TEXTURES_CATEGORIES_TBL_NAME;

        // Delete table.
        return $this->deleteTable($table);
    }

    /** Create colors table. */
    public function createColorsTable()
    {
        // Get table name.
        $table = $this->prefix . CAROUSEL_3D_COLORS_TBL_NAME;

        // Build query.
        $query = "CREATE TABLE `" . $table . "` (
            `ID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(50) NULL,
            `active` BOOLEAN,
            PRIMARY KEY (`ID`)
        ) COLLATE " . $this->collate . ";";

        // Create table.
        return $this->createTable($table, $query);
    }

    /** Delete textures table. */
    public function deleteColorsTable()
    {
        // Get table name.
        $table = $this->prefix . CAROUSEL_3D_COLORS_TBL_NAME;

        // Delete table.
        return $this->deleteTable($table);
    }

    /** Create products table. */
    public function createProductsTable()
    {
        // Get table name.
        $table = $this->prefix . CAROUSEL_3D_PRODUCTS_TBL_NAME;

        // Build query.
        $query = "CREATE TABLE `" . $table . "` (
            `ID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(250) NULL,
            `mtl` VARCHAR(50) NULL,
            `obj` VARCHAR(50) NULL,
            `category_id` INT(10) NULL,
            `collection_id` INT(10) NULL,
            `padding_x` INT(10) NULL,
            `padding_y` INT(10) NULL,
            `zoom` INT(10) NULL,
            `min_zoom` INT(10) NULL,
            `max_zoom` INT(10) NULL,
            `speed_rotate` INT(10) NULL,
            `shadow` BOOLEAN NULL,
            `active` BOOLEAN,
            PRIMARY KEY (`ID`)
        ) COLLATE " . $this->collate . ";";

        // Create table.
        return $this->createTable($table, $query);
    }

    /** Delete products table. */
    public function deleteProductsTable()
    {
        // Get table name.
        $table = $this->prefix . CAROUSEL_3D_PRODUCTS_TBL_NAME;

        // Delete table.
        return $this->deleteTable($table);
    }

    /** Create materials for the product table. */
    public function createProductMaterialsTable()
    {
        // Get table name.
        $table = $this->prefix . CAROUSEL_3D_PRODUCT_MATERIALS_TBL_NAME;

        // Build query.
        $query = "CREATE TABLE `" . $table . "` (
            `ID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `product_id` INT(10) NULL,
            `name` VARCHAR(250) NULL,
            `alias` VARCHAR(250) NULL,
            `textures` VARCHAR(500) NULL,
            `colors` VARCHAR(500) NULL,
            PRIMARY KEY (`ID`)
        ) COLLATE " . $this->collate . ";";

        // Create table.
        return $this->createTable($table, $query);
    }

    /** Delete materials for the product table. */
    public function deleteProductMaterialsTable()
    {
        // Get table name.
        $table = $this->prefix . CAROUSEL_3D_PRODUCT_MATERIALS_TBL_NAME;

        // Delete table.
        return $this->deleteTable($table);
    }

    /** Create categories for the product table. */
    public function createProductCategoriesTable()
    {
        // Get table name.
        $table = $this->prefix . CAROUSEL_3D_PRODUCT_CATEGORIES_TBL_NAME;

        // Build query.
        $query = "CREATE TABLE `" . $table . "` (
            `ID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(50) NULL,
            PRIMARY KEY (`ID`)
        ) COLLATE " . $this->collate . ";";

        // Create table.
        return $this->createTable($table, $query);
    }

    /** Delete categories for the product table. */
    public function deleteProductCategoriesTable()
    {
        // Get table name.
        $table = $this->prefix . CAROUSEL_3D_PRODUCT_CATEGORIES_TBL_NAME;

        // Delete table.
        return $this->deleteTable($table);
    }

    /** Delete collections for the product table. */
    public function createProductCollectionsTable()
    {
        // Get table name.
        $table = $this->prefix . CAROUSEL_3D_PRODUCT_COLLECTIONS_TBL_NAME;

        // Build query.
        $query = "CREATE TABLE `" . $table . "` (
            `ID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
            `name` VARCHAR(50) NULL,
            PRIMARY KEY (`ID`)
        ) COLLATE " . $this->collate . ";";

        // Create table.
        return $this->createTable($table, $query);
    }

    /** Delete collections for the product table. */
    public function deleteProductCollectionsTable()
    {
        // Get table name.
        $table = $this->prefix . CAROUSEL_3D_PRODUCT_COLLECTIONS_TBL_NAME;

        // Delete table.
        return $this->deleteTable($table);
    }

    /** Insert to table. */
    public function insert($tbl_name, $data, $format)
    {
        $table = $this->prefix . $tbl_name;

        $this->wpdb->insert($table, $data, $format);

        return $this->wpdb->insert_id;
    }

    /** Update to table. */
    public function update($tbl_name, $data, $format)
    {
        $table = $this->prefix . $tbl_name;

        return $this->wpdb->update($table, $data, $format);
    }

    /** Select all from table. */
    public function selectAll($query)
    {
        return $this->wpdb->get_results($query);
    }

    /** Select one from table. */
    public function select($query)
    {
        return $this->wpdb->get_results($query)[0] ?: null;
    }

    /** Select all from table. */
    public function delete($tbl_name, $conditions)
    {
        $table = $this->prefix . $tbl_name;

        return $this->wpdb->delete($table, $conditions);
    }

    /** Execute SQL query from DB. */
    public function query($query)
    {
        return $this->wpdb->query($query);
    }

    /** Get result SQL query from DB. */
    public function get($query)
    {
        return $this->wpdb->get_results($query);
    }
}