<?php

class ProductCategories extends BasicController
{
    public function route()
    {
        return parent::route();
    }

    /** Show all products. */
    public function index()
    {
        $uri = parent::getCurrentUri();

        $db = new DB();

        $categories = $db->selectAll('SELECT * FROM ' . $db->prefix . CAROUSEL_3D_PRODUCT_CATEGORIES_TBL_NAME);

        return parent::view('admin/product_categories/index.php', compact('categories', 'uri'));
    }

    /** Save product. */
    public function save($request)
    {
        // Save product to DB.
        $db = new \DB();
        $db->wpdb->query('TRUNCATE TABLE ' . $db->prefix . CAROUSEL_3D_PRODUCT_CATEGORIES_TBL_NAME);

        foreach ($request['categories'] as $categories) {
            $db->insert(CAROUSEL_3D_PRODUCT_CATEGORIES_TBL_NAME, [
                'name' => $categories['name'],
            ], [
                '%s'
            ]);
        }

        echo true;
    }

    /** Get all textures. */
    public function getAll($only_active = false)
    {
        $db = new DB();

        $active_concat = '';
        if($only_active) {
            $active_concat = ' WHERE active = 1';
        }

        return $db->selectAll('SELECT * FROM ' . $db->prefix . CAROUSEL_3D_PRODUCT_CATEGORIES_TBL_NAME . $active_concat);
    }
}