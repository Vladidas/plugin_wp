<?php

/** Declare basic plugin dir. */
define('CAROUSEL_3D_DIR', plugin_dir_path(__FILE__));

/** Declare views folder dir. */
define('CAROUSEL_3D_ADMIN_VIEW_FOLDER', CAROUSEL_3D_DIR . 'views/');
define('CAROUSEL_3D_ADMIN_ASSETS_FOLDER', CAROUSEL_3D_DIR . 'assets/');

define('CAROUSEL_3D_DIR_ASSET', CAROUSEL_3D_DIR .  'assets/');
define('CAROUSEL_3D_URL_ASSET', content_url() . '/plugins/3DCarousel/assets/');

/** Declare tables name. */
define('CAROUSEL_3D_TEXTURES_TBL_NAME', 'carousel_textures');
define('CAROUSEL_3D_TEXTURES_CATEGORIES_TBL_NAME', 'carousel_texture_categories');
define('CAROUSEL_3D_PRODUCT_MATERIALS_TBL_NAME', 'carousel_product_materials');
define('CAROUSEL_3D_PRODUCTS_TBL_NAME', 'carousel_products');
define('CAROUSEL_3D_PRODUCT_CATEGORIES_TBL_NAME', 'carousel_product_categories');
define('CAROUSEL_3D_PRODUCT_COLLECTIONS_TBL_NAME', 'carousel_product_collections');
define('CAROUSEL_3D_COLORS_TBL_NAME', 'carousel_colors');

/** Path to image gallery. */
define('CAROUSEL_3D_TEXTURES_DIR_GALLERY', wp_upload_dir()['basedir'] . '/3DCarousel/textures/');
define('CAROUSEL_3D_TEXTURES_URL_GALLERY', wp_upload_dir()['baseurl'] . '/3DCarousel/textures/');
define('CAROUSEL_3D_PRODUCTS_DIR_GALLERY', wp_upload_dir()['basedir'] . '/3DCarousel/products/');
define('CAROUSEL_3D_PRODUCTS_URL_GALLERY', wp_upload_dir()['baseurl'] . '/3DCarousel/products/');

/** Global settings menu admin-panel. */
$GLOBALS['admin_menu'] = [
    'name' => '3DCarousel',
    'class' => 'Products',
    'method' => 'route',
    'child' => [
        ['class' => 'Textures',           'method' => 'index', 'name' => 'Текстури'],
        ['class' => 'TextureCategories',  'method' => 'index', 'name' => 'Категорії текстур'],
        ['class' => 'Colors',             'method' => 'index', 'name' => 'Кольори'],
        ['class' => 'Products',           'method' => 'route', 'name' => 'Товари'],
        ['class' => 'ProductCollections', 'method' => 'index', 'name' => 'Колекції товару'],
        ['class' => 'ProductCategories',  'method' => 'index', 'name' => 'Категорії товару'],
    ]
];