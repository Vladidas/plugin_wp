// Add color.
$('#add-row').on('click', function() {
    $('#gallery tbody tr:first')
        .clone()
        .insertAfter('#gallery tbody tr:last')
        .fadeIn('fast')
        .find('td.js_show input[type="checkbox"]')
        .prop('checked', true);
});

// Remove color.
$(document).on('click', '.js_action', function() {
    $(this).closest('tr').fadeOut('fast', function () {
        $(this).remove();
    });
});

// Send to Back-end each other color.
$('button[value="submit"]').on('click', function () {
    var colors = [];
    $('#gallery tbody tr:visible').each(function(){
        colors.push({
            'name': $(this).find('td.js_color input[name="colors"]').val(),
            'active': $(this).find('td.js_show input[type="checkbox"]').prop('checked')
        })
    });

    $.ajax({
        url: document.URL,
        type: 'POST',
        data: {
            'class': 'Colors',
            'method': 'save',
            'colors': colors
        },
        success: function (response) {
            if(response) {
                $('#save_done').fadeIn('fast', function() {
                    var element = $(this);
                    setTimeout(function(){
                        element.fadeOut('slow');
                    }, 500);
                })
            }
        }
    });
});