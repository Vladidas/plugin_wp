// Add category.
$('#add-row').on('click', function() {
    $('#gallery tbody tr:first')
        .clone()
        .insertAfter('#gallery tbody tr:last')
        .fadeIn('fast')
        .find('td.js_show input[type="checkbox"]')
        .prop('checked', true);
});

// Remove category.
$(document).on('click', '.js_action', function() {
    $(this).closest('tr').fadeOut('fast', function () {
        $(this).remove();
    });
});

// Send to Back-end each other category.
$('button[value="submit"]').on('click', function () {
    var categories = [];
    $('#gallery tbody tr:visible').each(function(){
        categories.push({
            'name': $(this).find('td.js_category input[name="category"]').val(),
            'active': $(this).find('td.js_show input[type="checkbox"]').prop('checked')
        })
    });

    $.ajax({
        url: document.URL,
        type: 'POST',
        data: {
            'class': 'ProductCategories',
            'method': 'save',
            'categories': categories
        },
        success: function (response) {
            console.log(response);

            if(response) {
                $('#save_done').fadeIn('fast', function() {
                    var element = $(this);
                    setTimeout(function(){
                        element.fadeOut('slow');
                    }, 500);
                })
            }
        }
    });
});