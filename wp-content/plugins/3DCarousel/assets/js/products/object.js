function initObject() {
  	// Show download progress.
  var onProgress = function ( xhr ) {

      if ( xhr.lengthComputable ) {
        var percentComplete = xhr.loaded / xhr.total * 100;
        console.log( Math.round( percentComplete, 2 ) + '% downloaded' );

        if(percentComplete === 100) {
          // modelExist = true;
          // document.querySelector('.preloader').remove()
        }
      }
  };

  var onError = function () { };

  THREE.Loader.Handlers.add(/\.dds$/i, new THREE.DDSLoader());

  new THREE.MTLLoader()
    // .setPath('/')
    .load(mtlData, function (materials) {
      materials.preload();

      // let allMaterials = document.querySelectorAll('.js-default-material');
      // while (allMaterials.length > 1) {
      //     allMaterials.removeChild(allMaterials.lastChild);
      // }
      // TODO: REMOVE ALL .js-default-material EXCEPT FIRST!

      if (!modelExist) {
        for(let material in materials.materials) {

          const layoutMaterialBlock = document.querySelector('.js-default-material').cloneNode(true);
          layoutMaterialBlock.setAttribute("hidden", false)
          const newMaterialBlock = document.querySelector('.materials-info').appendChild(layoutMaterialBlock);
          newMaterialBlock.style.display = "block";

          layoutMaterialBlock.querySelector('.js-default-material[hidden="false"] input[type="text"]').setAttribute('value', materials.materials[material].name);
          layoutMaterialBlock.querySelector('.js-default-material[hidden="false"] input[type="hidden"]').setAttribute('value', materials.materials[material].name);

          // newMaterialBlock.classList.remove('js-default-material');
        }
        clickOnMaterials();
      }

      new THREE.OBJLoader()
        // .setPath('/')
        .setMaterials(materials)
        .load(objectData, function (object) {
          // object.position.y = 1;

          let defaultTexture = new THREE.TextureLoader()
              .setPath(pathToTextures)
              .load('default_texture.jpg');


          object.traverse(function(child){
            if (child instanceof THREE.Mesh) {
              child.material.color.setHex(defaultColor);
              child.material.map = defaultTexture;
              child.shadowMapNeedsUpdate = true;
              child.recieveShadow = false;
              child.castShadow = true;
            }
          });

          object.name = 'model';
          // ------------ RENDER
          scene.add(object);
        }, onProgress, onError);
    });
}
