function init() {

  initScene();

  // initObject();
  initMisc();

  container = document.querySelector('.container');
  container.appendChild(renderer.domElement);

  canvas = document.querySelector('canvas');
  bounds = canvas.getBoundingClientRect();

  container.style.width = `${canvas.offsetWidth}px`;
  container.style.height = `${canvas.offsetHeight}px`;



  document.addEventListener('mousemove', onDocumentMouseMove, false);
  window.addEventListener('resize', onWindowResize, false);


  // Click on single mesh of object.
  renderer.domElement.addEventListener('touchend', onDocumentTouch, false);
  renderer.domElement.addEventListener('click', onDocumentClick, false);

  // Turn off/on auto rotate.
  document.querySelector('.js-btn-rotate').addEventListener('click', rotateControl, false);
  document.querySelector('.js-btn-shadow').addEventListener('click', shadowToggle, false);


}

function initMisc() {
  renderer = new THREE.WebGLRenderer();
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth*0.5, window.innerHeight*0.5);
  renderer.shadowMap.enabled = true;
  renderer.shadowMap.needsUpdate = true;
  renderer.shadowMap.type = 2;
  document.body.appendChild(renderer.domElement);

  // controls
  controls = new THREE.OrbitControls( camera, renderer.domElement );
  controls.enableDamping = true;
  controls.dampingFactor = 0.25;

  controls.screenSpacePanning = false;
  controls.minDistance = 0;
  controls.maxDistance = 10000;
  controls.maxPolarAngle = Math.PI / 2;
  controls.center.set(0,0,0);
  controls.autoRotate = true;
  controls.autoRotateSpeed = 1.3;
}

function onWindowResize() {
  renderer.setSize( window.innerWidth, window.innerHeight );
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();
}

function onDocumentMouseMove(event) {
  event.preventDefault();

  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;
}

// Choose specific material of all object.
function chooseMaterial(event, mouse) {
  lastIndex = scene.children.length - 1;

  let targets = scene.children[lastIndex].children;
  let raycaster = new THREE.Raycaster();
  let intersects;


  raycaster.setFromCamera(mouse, camera);
  intersects = raycaster.intersectObjects(targets);
  


  if (intersects.length > 0) {

    intersects[0].object.material.color.setHex(0xff0000);

    activeMaterialName = intersects[0].object.material.name;
    activeMaterial = intersects[0].object.material;


    scene.children[lastIndex].traverse(function(child){
      if (child instanceof THREE.Mesh) {
          if(child.material.name === activeMaterialName) {
              child.material.color.setHex(0xff0000);
          } else {
           child.material.color.setHex(defaultColor);
          }
      }
  });

    let array = document.querySelectorAll('.materials-info li.js-default-material');
    let materialItems = [...array].splice(1);

    for(item in materialItems) {

      let itemMaterial = materialItems[item].querySelector('input[type="hidden"]').getAttribute('value');

      if (activeMaterialName === itemMaterial) {
         materialItems[item].classList.add('active-material');
      } else {
        materialItems[item].classList.remove('active-material')
      }
    }

  } 
  else {
    let array = document.querySelectorAll('.materials-info li.js-default-material');
    for (i = 0; i < array.length; i++){
      array[i].classList.remove('active-material');
  };

    for (material in scene.children[lastIndex].children) {
      scene.children[lastIndex].children[material].material.color.setHex(defaultColor);
    }
  }
}

document.addEventListener('click', function (e) {
  let arrayOfMaterials = document.querySelectorAll('.active-material');
  let choosedMaterial = [...arrayOfMaterials];

  
  if (arrayOfMaterials.length > 1) {
    for (i in choosedMaterial) {
        // console.log(choosedMaterial[i].classList.remove('active-material'))
        // console.log(choosedMaterial[choosedMaterial.length].classList.add('active-material'))
    }
  }
})



function onDocumentClick(event) {
  let mouse = new THREE.Vector2();

    mouse.x =  ( (event.clientX -  bounds.left) / canvas.clientWidth ) * 2 - 1;
    mouse.y = -( (event.clientY -  bounds.top) / canvas.clientHeight ) * 2 + 1;

  chooseMaterial(event, mouse);
}

function onDocumentTouch(event) {
  let mouse = new THREE.Vector2();

  mouse.x = +( (event.changedTouches[0].clientX - bounds.left)  / canvas.innerWidth ) * 2 +-1;
  mouse.y = -( (event.changedTouches[0].clientY -  bounds.top) / canvas.innerHeight ) * 2 + 1;

  chooseMaterial(event, mouse)
}

function rotateControl(event) {
  controls.autoRotate = !controls.autoRotate;
  controls.update();
}

// Turn on/off shadow of object.
function shadowToggle() {
  lastIndex = scene.children.length - 1;
  let object = scene.children[lastIndex].children

  for (mesh in object) {
    object[mesh].castShadow = !object[mesh].castShadow;
  }
}

function animate() {
  requestAnimationFrame(animate);
  controls.update();
  render();
}

function render() {
  camera.lookAt(scene.position);
  camera.updateMatrixWorld();
  renderer.render(scene, camera);
}
