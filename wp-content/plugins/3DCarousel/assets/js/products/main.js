// Set active material name status.
$(document).on('click', '.js-material-edit', function() {
    $(this).siblings('input[type="text"]').removeAttr('readonly');
});

// Set all button without active readonly status.
$(document).on('click', function(e) {
    if(!$(e.target).is('button[name="materials"]') || !$(e.target).is('button[name="materials"] *')) {
        $('button[name="materials"]').not($(e.target).closest('button[name="materials"]')).find('input[type="text"]').attr('readonly', true);
    }
});