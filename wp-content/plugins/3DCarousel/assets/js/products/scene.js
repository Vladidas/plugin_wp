function initScene() {

			camera = new THREE.PerspectiveCamera(2, window.innerWidth / window.innerHeight, 1, 12000);
			// camera = new THREE.OrthographicCamera( window.innerWidth  / - 2, window.innerWidth  / 2, window.innerHeight / 2, window.innerHeight / - 2, 1, 1000 );
      camera.position.set( 0, 10, 35 );

			scene = new THREE.Scene();
			let fogColor = new THREE.Color(0xffffff);
			scene.background = fogColor;

      scene.add( camera );


			
		// Lights
			spotLight = new THREE.SpotLight( 0xffffff, 0.5 );
				spotLight.name = 'Spot Light';
				spotLight.angle = Math.PI / 5;
				spotLight.penumbra = 0.3;
				spotLight.position.set( 5, 5, 5 );
				scene.add( spotLight );

			dirLight = new THREE.DirectionalLight( 0xffffff, 0.6 );
				dirLight.name = 'Dir. Light';
				dirLight.position.set( 0, 10, 0 );
				dirLight.castShadow = true;

				dirLight.shadow.camera.near = 0.1;
				dirLight.shadow.camera.far = 100;

				dirLight.shadow.camera.right = 20;
				dirLight.shadow.camera.left = - 20;
				dirLight.shadow.camera.top	= 10;
				dirLight.shadow.camera.bottom = -10;

				dirLight.shadow.mapSize.width = 1024;
        dirLight.shadow.mapSize.height = 1024;
				dirLight.shadow.radius = .5;
				
				scene.add( dirLight );

      var ambientLight = new THREE.AmbientLight(0xd2d2d2, 0.7);
      scene.add(ambientLight);


			var geometry = new THREE.BoxBufferGeometry( 5, 0.01, 5 );
			var material = new THREE.MeshLambertMaterial( {
				color: 0xffffff,
				shininess: 150,
				specular: 0xa3a3a3
			});


			var ground = new THREE.Mesh( geometry, material );
				ground.name = 'Ground';
				ground.castShadow = false;
        ground.receiveShadow = true;
				scene.add( ground );
}