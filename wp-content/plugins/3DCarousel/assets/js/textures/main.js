// Get all images.
var files = []; var fileId = 0;
$('input[name="images[]"]').on('change', function() {

    // Loop all images to each other image.
    for (var i = 0; i < this.files.length; i++) {
        // Check the file type.
        if (this.files[i].type.match('image.*')) {
            files.push({
                id: fileId++,
                file: this.files[i]
            });

            // Read and attach for the sending image as file.
            showImage(this.files[i], function (image) {
                preview(image);
            })
        }
    }
});

// Get preview image having data as file.
function showImage(file, callback) {
    var reader = new FileReader();

    reader.onload = function(event) {
        return callback(event.target.result);
    };

    reader.readAsDataURL(file);
}

// Append to result table info
function preview(image) {
    const id =  $('#gallery tbody tr').length - 1;
    const newTr = $('#gallery tbody tr:first').clone().insertAfter('#gallery tbody tr:last').attr('data-id', id).fadeIn('fast');
    newTr.find('.js_image img').attr('src', image);
    newTr.find('.js_show input').attr('checked', true);
}

// Delete image.
$(document).on('click', '.js_action button', function() {
    const indexTr = $(this).closest('tr').index() - 1;

    $(this).closest('tr').fadeOut('fast', function () {
        files.splice(indexTr, 1);
        $(this).remove();
    })
});

// Send to Back-end each other image.
$('button[value="submit"]').on('click', function () {
    var formData = new FormData();

    // Get old images and active status.
    $('table tbody input[name="image_old"]').each(function() {
        const isActive = $(this).closest('tr').find('.js_show input[type="checkbox"]').prop('checked');
        const categoryID = $(this).closest('tr').find('select[name="category_id"] option:selected').val();

        formData.append('old_images_name[]', $(this).val());
        formData.append('old_images_active[]', isActive);
        formData.append('old_images_category_id[]', categoryID);
    });

    // Get new images and active status.
    for (var i = 0; i < files.length; i++) {
        const isActive = $('#gallery tbody tr:not(.js-old-images):not(:first):eq(' + (i) + ')').find('input:checkbox').prop('checked');
        const categoryID = $('#gallery tbody tr:not(.js-old-images):not(:first):eq(' + (i) + ')').find('select option:selected').val();

        formData.append('images[]', files[i].file);
        formData.append('active[]', isActive);
        formData.append('category_id[]', categoryID);
    }

    // Append class and method action for the route.
    formData.append('class', 'Textures');
    formData.append('method', 'save');

    $.ajax({
        url: document.URL,
        type: 'POST',
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        success: function (response) {
            console.log(response)

            if(response) {
                $('#save_done').fadeIn('fast', function() {
                    var element = $(this);
                    setTimeout(function(){
                        element.fadeOut('slow');
                    }, 500);
                })
            }
        }
    });
});