<?php
/**
 * @package 3DCarousel
 */
/*
Plugin Name: 3D Carousel
Plugin URI: https://bluebird.team/
Description: 3D full-stack pack plugin which rotate, change colors and textures. Also him has a powerfull with Admin panel. All codes support PHP 5.6+ and was wrote at OOP-style
Version: 1.0
Author: Vladidas
Author URI: https://bluebird.team/
License: None
Text Domain: 3DCarousel
*/

/*
TODO: Descriptions about plugin.
*/

require_once plugin_dir_path(__FILE__) . '/config.php';

// If is request admin-panel.
if(!empty($_POST)) {
    require_all();

    $request = $_POST;
    $class = $request['class'] ?: null;
    $method = $request['method'] ?: null;

    if($class && $method) {
        (new $class)->$method($request);
        die;
    }
}

// If use this plugin admin role.
if (is_admin()) {

    require_all();

    // Declare install class for operation.
    $install = new Install;

    // Build admin menu plugin.
    $install->buildAdminMenu();

    // Activate plugin.
    register_activation_hook(__FILE__, [$install, 'activate']);

    // Deactivate plugin.
    register_deactivation_hook(__FILE__, [$install, 'deactivate']);

    // Uninstall plugin.
    register_uninstall_hook(__FILE__, array($install, 'uninstall') );

} else {
    require_once plugin_dir_path(__FILE__) . '/includes/classes/admin/DB.php';
    require_once plugin_dir_path(__FILE__) . '/includes/classes/admin/BasicController.php';
    require_once plugin_dir_path(__FILE__) . '/includes/classes/admin/Products.php';
    require_once plugin_dir_path(__FILE__) . '/includes/classes/user/Carousel.php';

    // Add styles and scripts.
//    add_action('wp_print_scripts', array(&$this, 'site_load_scripts'));
//    add_action('wp_print_styles', array(&$this, 'site_load_styles'));
}

// Require all files.
function require_all() {
    require_once plugin_dir_path(__FILE__) . '/includes/classes/admin/Carousel.php';
    require_once plugin_dir_path(__FILE__) . '/includes/classes/admin/BasicController.php';
    require_once plugin_dir_path(__FILE__) . '/includes/classes/admin/DB.php';
    require_once plugin_dir_path(__FILE__) . '/includes/classes/admin/Colors.php';
    require_once plugin_dir_path(__FILE__) . '/includes/classes/admin/Install.php';
    require_once plugin_dir_path(__FILE__) . '/includes/classes/admin/Products.php';
    require_once plugin_dir_path(__FILE__) . '/includes/classes/admin/ProductCategories.php';
    require_once plugin_dir_path(__FILE__) . '/includes/classes/admin/ProductCollections.php';
    require_once plugin_dir_path(__FILE__) . '/includes/classes/admin/Textures.php';
    require_once plugin_dir_path(__FILE__) . '/includes/classes/admin/TextureCategories.php';
}