<!DOCTYPE html>
<html lang="en">

<head>
    <title>Carousel Plugin</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="<?= CAROUSEL_3D_URL_ASSET ?>css/style.css">
</head>

<body>

<?php $id = 2; ?>
<?php $product = \user\Carousel::show(['id' => $id]); ?>

<?php if(!$product->mtl || !$product->obj) die('Немає .mtl або .obj файлу!'); ?>

<div class="viewer-wrapper">
    <div class="container"></div>
        <button id="start" type="button" name="button">Start</button>
        <button type="button" class="btn js-btn-rotate">Turn off/on auto rotate</button>
        <input id="paddingX" name="padding_x" type="number" value="<?= $product->padding_x ?: 1 ?>" min="1" max="1000">
        <input id="paddingY" name="padding_y" type="number" value="<?= $product->padding_y ?: 1 ?>" min="1" max="1000">
        <input type="checkbox" name="shadow" id="shadow" class="btn js-btn-shadow" <?= $product->shadow == 1 ? 'checked' : '' ?>>
        <input id="zoom" name="zoom" type="number" value="<?= $product->zoom ?: 1 ?>" min="1" max="10000">
        <input id="minZoom" name="min_zoom" type="number" value="<?= $product->min_zoom ?: 1 ?>" min="1" max="10000">
        <input id="maxZoom" name="max_zoom" type="number" value="<?= $product->max_zoom ?: 1000 ?>" min="1" max="10000">
    </div>
</div>

<ol class="materials-info">
    <li class="default-material js-default-material"></li>
</ol>

<?php if ($product->materials): ?>
    <?php foreach ($product->materials as $material): ?>
        <ul>

            <?php if ($material['textures']): ?>
                <?php foreach ($material['textures'] as $texture): ?>
                    <li><img src="<?= CAROUSEL_3D_TEXTURES_URL_GALLERY . $texture->name ?>"></li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>

        <ul>
            <?php if ($material['colors']): ?>
                <?php foreach ($material['colors'] as $color): ?>
                    <li style="background-color: <?= $color->name ?>"><?= $color->name ?></li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    <?php endforeach; ?>
<?php endif; ?>

<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/libs/three.min.js"></script>

<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/libs/OrbitControls.js"></script>
<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/libs/WebGL.js"></script>

<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/libs/DDSLoader.js"></script>
<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/libs/MTLLoader.js"></script>
<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/libs/OBJLoader.js"></script>

<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/products/scene.js"></script>
<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/products/object.js"></script>
<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/products/color.js"></script>
<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/products/plugin.js"></script>

<script>
    let container, stats;
    let object;
    let camera, scene, controls;

    let mouse = new THREE.Vector2(), INTERSECTED;

    let activeMaterial, activeMaterialName;

    let dirLightShadowMapViewer, spotLightShadowMapViewer;

    let dirLight, spotLight;

    let canvas, bounds;

    let lastIndex;

    let mtlData, objectData;

    let zoom, minZoom, maxZoom;

    mtlData = '<?= CAROUSEL_3D_PRODUCTS_URL_GALLERY . $product->mtl ?>';
    objectData = '<?= CAROUSEL_3D_PRODUCTS_URL_GALLERY . $product->obj ?>';

    document.addEventListener('DOMContentLoaded', initilaisationModel, false);

    let maxDistance, rotateSpeed;

    const pathToTextures = '<?= $path_to_textures ?>';

    document.getElementById('start').addEventListener('click', initilaisationModel, false);

    init();
    animate();
    // initMisc();
    function initilaisationModel () {
        console.log(mtlData, objectData)

        if(!mtlData || !objectData) {
            return alert("Select files")
        }
        
        initObject();

        // if (mtlData && objectData) {
        // lastIndex = scene.children.length - 1;
        // scene.children.splice(lastIndex, 1)
        // }
        // init();
        // document.querySelector('.initial-content').style.display = 'none';
    }

    document.getElementById('mtl').addEventListener('change', function (e) {
        let file = this.files[0];
        mtlData = window.URL.createObjectURL(file);

        console.log(mtlData)
        return mtlData
    });

    document.getElementById('object').addEventListener('change', function (e) {
        let file = this.files[0];
        objectData = window.URL.createObjectURL(file);

        console.log(objectData)
        return objectData
    });

    document.getElementById('maxZoom').addEventListener('keyup', function(e){
        e.preventDefault();

        maxZoom = this.value;
        controls.maxDistance = this.value;
        controls.update();
    });

    document.getElementById('minZoom').addEventListener('keyup', function(e){
        e.preventDefault();

        minZoom = this.value
        controls.minDistance = this.value;
        controls.update();
        // camera.zoom = this.value;
    });

    document.getElementById('zoom').addEventListener('keyup', function(e){
        e.preventDefault();

        zoom = this.value;
        camera.position.z = this.value;
        controls.update();
    });

    document.querySelector('.materials-info').addEventListener('mousemove', function(e){
        e.preventDefault();
    })
</script>

</body>

</html>

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="<?= CAROUSEL_3D_URL_ASSET ?>/js/products/main.js"></script>

<?php //get_header(); ?>
<?php //get_footer(); ?>
